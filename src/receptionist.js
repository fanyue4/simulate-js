// TODO: Write a class `Receptionist`. Please review the code
// in the unit test to understand its usage.
// <-start-
class Receptionist {
  constructor (persons = [], ids = []) {
    this.persons = persons;
    this.ids = ids;
  }

  register (person) {
    if (person === '' || person === null || person === undefined || person === {} || {}.hasOwnProperty.call(person, 'id') === false || {}.hasOwnProperty.call(person, 'name') === false) {
      throw new Error('Invalid person');
    } else if (this.ids.includes(person.id) === true) {
      throw new Error('Person already exist');
    } else {
      this.persons.push(person);
      this.ids.push(person.id);
    }
    return person;
  }

  getPerson (personId) {
    if (this.ids.includes(personId) === false) {
      throw new Error('Person not found');
    }

    for (let i = 0; i < this.persons.length; i++) {
      if (this.persons[i].id === personId) {
        return this.persons[i];
      }
    }
  }
}
// --end-->

export default Receptionist;
