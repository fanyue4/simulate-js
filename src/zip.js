// TODO: Write a function `zip`. Please review the code in
// the unit test to understand its usage.
// <-start-
function zip (left, right) {
  const result = [];

  if (left.length <= right.length) {
    for (let i = 0; i < left.length; i++) {
      result.push([left[i], right[i]]);
    }
  } else if (left.length === 0 || right.length === 0) {

  } else if (left.length > right.length) {
    for (let i = 0; i < right.length; i++) {
      result.push([left[i], right[i]]);
    }
  }

  return result;
}
// --end->

export default zip;
